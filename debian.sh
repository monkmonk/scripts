#!/bin/bash
# add -y to run yabs when running this script

#update and upgrade
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install vim docker.io docker-compose pip python3 net-tools apparmor htop tzdata -y
sudo apt-get autoremove -y

# set timezone 
sudo timedatectl set-timezone Asia/Bangkok

# install latest rclone
sudo -v ; curl https://rclone.org/install.sh | sudo bash

# enable bbr for faster internet
#sudo echo $'net.core.default_qdisc=fq\nnet.ipv4.tcp_congestion_control=bbr' > /etc/sysctl.d/10-custom-kernel-bbr.conf
echo "Setting up BBR..."
sudo sh -c "echo 'net.core.default_qdisc=fq' > /etc/sysctl.d/10-custom-kernel-bbr.conf"
sudo sh -c "echo 'net.ipv4.tcp_congestion_control=bbr' >> /etc/sysctl.d/10-custom-kernel-bbr.conf"
echo "BBR setup complete!"
sudo sysctl --system
sudo sysctl net.ipv4.tcp_congestion_control

# # create user and give them control of swag and comp
# sudo useradd -m sftp
# sudo mkdir -p /var/{swag,comp}
# sudo chown sftp:sftp /var/{swag,comp}

# # display sftp userid
# echo "sftp user id: "
# id -u sftp

# download yabs
cd 
git clone https://github.com/masonr/yet-another-bench-script.git
#cd yet-another-bench-script
echo "To run yabs type: curl -sL yabs.sh | bash"

