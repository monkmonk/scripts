#!/bin/bash
sudo apt-get update
sudo do-release-upgrade -y
sudo apt-get dist-upgrade -y
sudo apt-get upgrade -y
sudo apt-get install vim docker.io docker-compose git snapd net-tools pip python3 htop tzdata unzip borgbackup ubuntu-release-upgrader-core -y
sudo apt-get autoremove -y

sudo timedatectl set-timezone Asia/Bangkok
sudo -v ; curl https://rclone.org/install.sh | sudo bash
# sudo useradd -m sftp
# sudo mkdir /var/swag
# sudo mkdir /var/comp
# sudo chown sftp:sftp /var/comp
# sudo chown sftp:sftp /var/swag
git clone https://github.com/masonr/yet-another-bench-script.git
cd yet-another-bench-script
echo "To use yabs type: curl -sL yabs.sh | bash"
